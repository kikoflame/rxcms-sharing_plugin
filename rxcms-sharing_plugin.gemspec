$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "rxcms-sharing_plugin/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "rxcms-sharing_plugin"
  s.version     = RxcmsSharingPlugin::VERSION
  s.authors     = ["Anh Nguyen","Julian Cowan"]
  s.email       = ["rxcms@ngonluakiko.com"]
  s.homepage    = "https://bitbucket.org/kikoflame/rxcms-sharing_plugin"
  s.summary     = "Plugin that helps users create sharable content."
  s.description = "Extension to RXCMS that enables users to share content."
  s.licenses    = ["MIT"]

  s.metadata = {
      "compatCode" => "2dd89a9ecdeb920d539954fd9fb3da7de9f52a61ee9895d06e4bc1bab11a598b",
      "fullName" => "Sharing Plugin",
      "mountPoint" => "/sharing/engine/configure",
      "installerPoint" => "/sharing/engine/installer"
  }

  s.files = Dir["{app,config,db,lib}/**/*"] + ["LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 3.2.13"
  # s.add_dependency "jquery-rails"

  s.add_development_dependency "sqlite3"
end
