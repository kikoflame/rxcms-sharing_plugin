require "spec_helper"

describe SharingServicesController do
  before do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())
    TUserSession = Struct.new(:record)
    TUserSessionAttrs = Struct.new(:id)
    TUser = Struct.new(:login,:email)

    UserSession.stub(:find).and_return TUserSession.new(TUserSessionAttrs.new(1))
    User.stub(:find).and_return TUser.new('admin','test@test.com')

    stub_const("Role", double())
    stub_const("RxcmsCompliancePlugin", double())
    stub_const("Release", double())
    TUserRole = Struct.new(:name)
    TRelease = Struct.new(:id,:users_id)
    TUserSessionUser = Struct.new(:id)
  end

  it "should set release shared" do
    Release.should_receive(:find_by_metadata_id).and_return TRelease.new(1,1)
    UserSession.stub(:find) { UserSession }
    UserSession.should_receive(:record).at_least(:once).and_return TUserSessionUser.new(1)
    Release.should_receive(:update).and_return true

    post :set_release_shared, { :shared => true }

    response.should be_success
  end

  it "should get release shared" do
    Release.should_receive(:find_by_metadata_id).at_least(:once).and_return TRelease.new(1,1)

    get :get_release_shared, :id => 1

    response.should be_success
  end

  it "should get shared releases" do
    Release.stub(:select) { Release }
    Release.stub(:joins) { Release }
    Release.stub(:where) { Release }
    Release.stub(:offset) { Release }
    Release.should_receive(:limit).at_least(:once).and_return []
    UserSession.stub(:find) { UserSession }
    UserSession.should_receive(:record).at_least(:once).and_return TUserSessionUser.new(1)

    get :get_shared_releases, { :limit => 3, :offset => 1 }

    response.should be_success
  end

  it "should get shared item content" do
    stub_const("Metadata", double())
    TMetadata = Struct.new(:value)

    Metadata.should_receive(:find).and_return TMetadata.new('some value')

    get :get_shared_item_content, :id => 1

    response.should be_success
  end
end