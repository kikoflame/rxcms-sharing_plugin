require 'spec_helper'

describe SharingEngineController do
  before do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())
    TUserSession = Struct.new(:record)
    TUserSessionAttrs = Struct.new(:id)
    TUser = Struct.new(:login,:email)

    UserSession.stub(:find).and_return TUserSession.new(TUserSessionAttrs.new(1))
    User.stub(:find).and_return TUser.new('admin','test@test.com')
  end

  describe "#GET index" do
    it "should render readme page" do
      get :index
      response.should be_success
    end
  end

  describe "#GET configure" do
    before do
      stub_const("Role", double())
      TUserRole = Struct.new(:name)
    end

    it "should render configuration page" do
      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1

      controller.request.should_receive(:xhr?).at_least(:once).and_return true
      Role.should_receive(:find).and_return TUserRole.new('admin')

      get :configure
      response.body.should_not be_empty
    end

    it "should not render configuration page if user is not admin" do
      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1

      controller.request.should_receive(:xhr?).at_least(:once).and_return true
      Role.should_receive(:find).and_return TUserRole.new('user')

      expect { get :configure }.to raise_error
    end

    it "should not render configuration page if request is not xhr" do
      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1

      controller.request.should_receive(:xhr?).at_least(:once).and_return false
      Role.should_receive(:find).and_return TUserRole.new('admin')

      expect { get :configure }.to raise_error
    end
  end

  describe "#GET installer" do
    before do
      stub_const("Role", double())
      TUserRole = Struct.new(:name)
    end

    it "should render installer page" do
      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1

      controller.request.should_receive(:xhr?).at_least(:once).and_return true
      Role.should_receive(:find).and_return TUserRole.new('admin')

      get :installer
      response.body.should_not be_empty
    end

    it "should not render installer page if user is not admin" do
      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1

      controller.request.should_receive(:xhr?).at_least(:once).and_return true
      Role.should_receive(:find).and_return TUserRole.new('user')

      expect { get :installer }.to raise_error
    end

    it "should not render configuration page if request is not xhr" do
      session[:accessible_appid] = 1
      session[:accessible_roleid] = 1

      controller.request.should_receive(:xhr?).at_least(:once).and_return false
      Role.should_receive(:find).and_return TUserRole.new('admin')

      expect { get :installer }.to raise_error
    end
  end
end