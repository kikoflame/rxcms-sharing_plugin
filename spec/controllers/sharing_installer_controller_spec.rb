require 'spec_helper'

describe SharingInstallerController do
  before do
    stub_const("UserSession", double())
    stub_const("User", double())
    stub_const("Metadata", double())
    TUserSession = Struct.new(:record)
    TUserSessionAttrs = Struct.new(:id)
    TUser = Struct.new(:login,:email)

    UserSession.stub(:find).and_return TUserSession.new(TUserSessionAttrs.new(1))
    User.stub(:find).and_return TUser.new('admin','test@test.com')

    stub_const("Role", double())
    TUserRole = Struct.new(:name)
  end

  it "should before process" do
    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1

    Role.should_receive(:find).and_return TUserRole.new('admin')

    post :before_process
    response.should be_success
  end

  it "should not before process if unauthorized" do
    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1

    Role.should_receive(:find).and_return TUserRole.new('user')

    expect { post :before_process }.to raise_error
  end

  it "should uninstall" do
    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1

    Role.should_receive(:find).and_return TUserRole.new('admin')

    post :uninstall
    response.should be_success
  end

  it "should not uninstall if unauthorized" do
    session[:accessible_appid] = 1
    session[:accessible_roleid] = 1

    Role.should_receive(:find).and_return TUserRole.new('user')

    post :uninstall
    response.should be_success
  end
end