class SharingServicesController < ApplicationController
  include SharingPluginHelper

  layout false

  before_filter :get_current_user_role
  before_filter :check_compliance_presence, :except =>
  [
      :get_shared_releases,
      :get_shared_item_content
  ]

  # Load configuration items (MANDATORY, must be included)
  APP_CONFIG = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/sharing/sharing_config.yml', __FILE__))))

  def set_release_shared
    begin
      item = Release.find_by_metadata_id(params[:id].to_i)
      if (item.nil?)
        render :json => { :status => 'failure', :message => 'element has not been released yet' }
      else
        if (UserSession.find.record.id != item.users_id)
          render :json => { :status => 'failure', :message => "item sharing setings hasn't been changed because you are not the last publisher" }
        else
          Release.update(item.id, { :scope => params[:shared] })
          render :json => { :status => 'success' }
        end
      end
    rescue Exception => ex
      # render :json => { :status => 'failure', :message => ex.message }
      render :json => { :status => 'failure', :message => "unable to process your request" }
    end
  end

  def get_release_shared
    begin
      render :json => { :status => 'success', :data => Release.find_by_metadata_id(params[:id]).nil? ? nil : Release.find_by_metadata_id(params[:id]).scope }
    rescue Exception => ex
      render :json => { :status => 'failure', :message => "unable to process your request" }
    end
  end

  def get_shared_releases
    begin
      limit = params[:limit].to_i
      offset = params[:offset].to_i

      render :json => { :status => 'success', :data => Release.select("metadata.id, key, cat, value, login as name")
                                                              .joins("INNER JOIN metadata on metadata.id = releases.metadata_id")
                                                              .joins("INNER JOIN users on users.id = releases.users_id")
                                                              .where("scope <> ? and users_id <> ?", "w", UserSession.find.record.id).offset(offset).limit(limit) }
    rescue Exception => ex
      render :json => { :status => 'failure', :message => "unable to process your request" }
    end
  end

  def get_shared_item_content
    begin
      render :json => { :status => 'success', :data => Metadata.find(params[:id]).value }
    rescue Exception => ex
      render :json => { :status => 'failure', :message => "unable to process your request" }
    end
  end

  private

  def check_compliance_presence
    if (!defined?(RxcmsCompliancePlugin))
      raise
    end
  end
end
