class SharingInstallerController < ApplicationController
  include SharingPluginHelper

  layout false

  before_filter :get_current_user_role

  # Load configuration items (MANDATORY, must be included)
  APP_CONFIG = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/sharing/sharing_config.yml', __FILE__))))

  def before_process
    # Check access
    if (@curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'loggedin' ||
        @curUserRole == 'anonymous')
      raise 'unauthorized access'
    end

    render :json => { :status => 'unimplemented' }
  end

  def core_process
    render :json => { :status => 'unimplemented' }
  end

  def post_process
    render :json => { :status => 'unimplemented' }
  end

  def uninstall
    begin
      # Check access
      if (@curUserRole == 'contentadmin' ||
          @curUserRole == 'user' ||
          @curUserRole == 'loggedin' ||
          @curUserRole == 'anonymous')
        raise 'unauthorized access'
      end

      render :json => { :status => 'success' }
    rescue
      render :json => { :status => 'failure' }
    end
  end
end
