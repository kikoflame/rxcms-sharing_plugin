class SharingEngineController < ApplicationController
  include SharingPluginHelper

  layout false

  before_filter :get_current_user_role

  # Load configuration items (MANDATORY, must be included)
  APP_CONFIG = HashWithIndifferentAccess.new(YAML.load(File.read(File.expand_path('../../../config/sharing/sharing_config.yml', __FILE__))))

  def index

  end

  def configure
    # Check access
    if (@curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'loggedin' ||
        @curUserRole == 'anonymous')
      raise 'unauthorized access'
    end

    if request.xhr?
      respond_to do |t|
        t.html
      end
    else
      raise 'unauthorized access'
    end
  end

  def installer
    # Check access
    if (@curUserRole == 'contentadmin' ||
        @curUserRole == 'user' ||
        @curUserRole == 'loggedin' ||
        @curUserRole == 'anonymous')
      raise 'unauthorized access'
    end

    if request.xhr?
      respond_to do |t|
        t.html
      end
    else
      raise 'unauthorized access'
    end
  end
end
