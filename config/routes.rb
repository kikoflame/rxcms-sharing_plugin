Rails.application.routes.draw do

  get "/sharing/engine/readme" => "sharing_engine#index"
  get "/sharing/engine/configure" => "sharing_engine#configure"
  get "/sharing/engine/installer" => "sharing_engine#installer"

  post "/sharing/installer/before_process" => "sharing_installer#before_process"
  post "/sharing/installer/core_process" => "sharing_installer#core_process"
  post "/sharing/installer/post_process" => "sharing_installer#post_process"
  post "/sharing/installer/uninstall" => "sharing_installer#uninstall"

  post "/sharing/release/status" => "sharing_services#set_release_shared"
  get "/sharing/release/status/:id" => "sharing_services#get_release_shared"
  get "/sharing/release/shared/all/:offset/:limit" => "sharing_services#get_shared_releases"
  get "/sharing/release/shared/one/:id" => "sharing_services#get_shared_item_content"
end
